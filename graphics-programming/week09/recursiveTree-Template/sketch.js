// Code from a modified Daniel Shiffman example
// https://thecodingtrain.com/

var angle = 0;
var seed;

function setup() {
  createCanvas(400, 400);
  seed = random(1000);
}
////////////////////////////////////////////////
function draw() {
  background(225);
  angleMode(DEGREES);
  randomSeed(seed);
  angle = random(20, 30);
  stroke(255);
  translate(200, height);
  branch(100, 3);
}
/////////////////////////////////////////////////
function branch(len, thickness) {
  var r = map(len, 4, 100, 181, 43);
  var g = map(len, 4, 100, 101, 29);
  var b = map(len, 4, 100, 29, 20);
  stroke(r, g, b);

  strokeWeight(thickness);
  line(0, 0, 0, -len);
  translate(0, -len);
  var n = noise(frameCount / 60);
  if (len > 4) {
    push();
    rotate(angle - n * 20);
    branch(len * random(0.5, 0.8), thickness * 0.8);
    pop();
    push();
    rotate(-angle - n * 20);
    branch(len * random(0.5, 0.8), thickness * 0.8);
    pop();
  } else {
    push();
    fill(255, 0, 0);
    ellipse(0, 0, 10, 10);
    pop();
  }
}
