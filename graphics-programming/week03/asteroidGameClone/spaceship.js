class Spaceship {
  constructor() {
    this.velocity = new createVector(0, 0);
    this.location = new createVector(width / 2, height / 2);
    this.acceleration = new createVector(0, 0);
    this.maxVelocity = 5;
    this.bulletSys = new BulletSystem();
    this.size = 50;
  }

  run() {
    this.bulletSys.run();
    this.draw();
    this.move();
    this.edges();
    this.interaction();
  }

  getAngle() {
    if (keyIsDown(UP_ARROW)) {
      if (keyIsDown(RIGHT_ARROW)) {
        return PI / 4;
      }
      if (keyIsDown(LEFT_ARROW)) {
        return -PI / 4;
      }
      return 0;
    }

    if (keyIsDown(DOWN_ARROW)) {
      if (keyIsDown(RIGHT_ARROW)) {
        return (3 * PI) / 4;
      }
      if (keyIsDown(LEFT_ARROW)) {
        return (-3 * PI) / 4;
      }
      return PI;
    }

    if (keyIsDown(LEFT_ARROW)) {
      return -PI / 2;
    }

    if (keyIsDown(RIGHT_ARROW)) {
      return PI / 2;
    }
  }
  draw() {
    push();
    fill(0, 0, 255);
    translate(this.location.x, this.location.y);
    rotate(this.getAngle());
    rect(-this.size / 2, -this.size / 2, this.size, this.size);
    fill("green");
    triangle(
      -this.size / 2,
      -this.size / 2,
      this.size / 2,
      -this.size / 2,
      0,
      (-this.size * 3) / 4
    );
    if (this.isMoving()) {
      push();
      translate(-this.size / 4, 0);
      fill(255, 100, 0);
      triangle(
        -this.size / 6,
        this.size / 2,
        this.size / 6,
        this.size / 2,
        0,
        this.size
      );
      pop();
      push();
      fill(255, 100, 0);
      translate(this.size / 4, 0);
      triangle(
        -this.size / 6,
        this.size / 2,
        this.size / 6,
        this.size / 2,
        0,
        this.size
      );
      pop();
    }
    pop();
  }

  isMoving() {
    if (
      keyIsDown(LEFT_ARROW) ||
      keyIsDown(RIGHT_ARROW) ||
      keyIsDown(UP_ARROW) ||
      keyIsDown(DOWN_ARROW)
    ) {
      return true;
    }

    return false;
  }
  move() {
    // YOUR CODE HERE (4 lines)
    this.velocity.add(this.acceleration);
    this.location.add(this.velocity);
    this.acceleration.mult(0);
    this.velocity.limit(5);
  }

  applyForce(f) {
    this.acceleration.add(f);
  }

  interaction() {
    if (keyIsDown(LEFT_ARROW)) {
      this.applyForce(createVector(-0.1, 0));
    }
    if (keyIsDown(RIGHT_ARROW)) {
      // YOUR CODE HERE (1 line)
      this.applyForce(createVector(0.1, 0));
    }
    if (keyIsDown(UP_ARROW)) {
      // YOUR CODE HERE (1 line)
      this.applyForce(createVector(0, -0.1));
    }
    if (keyIsDown(DOWN_ARROW)) {
      // YOUR CODE HERE (1 line)
      this.applyForce(createVector(0, 0.1));
    }
  }

  fire() {
    this.bulletSys.fire(this.location.x, this.location.y);
  }

  edges() {
    if (this.location.x < 0) this.location.x = width;
    else if (this.location.x > width) this.location.x = 0;
    else if (this.location.y < 0) this.location.y = height;
    else if (this.location.y > height) this.location.y = 0;
  }

  setNearEarth() {
    //YOUR CODE HERE (6 lines approx)
    var downwardsPointing = new createVector(0, 0.05);
    var friction = this.velocity.copy();
    friction.mult(-1);
    friction.div(30);
    this.applyForce(downwardsPointing);
    this.applyForce(friction);
  }
}
