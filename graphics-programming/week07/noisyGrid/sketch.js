var stepSize = 20;

function setup() {
  createCanvas(500, 500);
}
///////////////////////////////////////////////////////////////////////
function draw() {
  background(125);

  colorGrid();
  compassGrid();
}
///////////////////////////////////////////////////////////////////////
function colorGrid() {
  // your code here
  let scale = map(mouseX, 0, width, 20, 200);
  for (var x = 0; x < width; x += stepSize) {
    for (var y = 0; y < height; y += stepSize) {
      var n = noise(x / scale, y / scale, frameCount / scale);
      let inter = lerpColor(color(255, 150, 0), color(100, 0, 255), n);
      fill(inter);
      strokeWeight(0);
      rect(x, y, stepSize, stepSize);
    }
  }
}
///////////////////////////////////////////////////////////////////////
function compassGrid() {
  // your code here
  strokeWeight(2);
  angleMode(DEGREES);
  let scale = map(mouseX, 0, width, 20, 200);
  for (var x = 0; x < width; x += stepSize) {
    for (var y = 0; y < height; y += stepSize) {
      var n = noise(x / scale, y / scale, frameCount / scale);
      var angle = map(n, 0, 1, 0, 720);
      push();
      translate(x + stepSize / 2, y + stepSize / 2);
      rotate(angle);
      line(0, (-stepSize / 2) * n, 0, (stepSize / 2) * n);
      pop();
    }
  }
}
