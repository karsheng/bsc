class Grid {
  /////////////////////////////////
  constructor(_w, _h, sounds) {
    this.gridWidth = _w;
    this.gridHeight = _h;
    this.noteSize = 40;
    this.notes = [];
    this.sounds = sounds;

    // initalise grid structure and state
    for (var x = 0; x < _w; x += this.noteSize) {
      var notesColumn = [];
      for (var y = 0; y < _h; y += this.noteSize) {
        var sound = this.sounds[int(random(this.sounds.length))];
        var note = new Note(x, y, this.noteSize, sound);
        notesColumn.push(note);
      }
      this.notes.push(notesColumn);
    }
  }
  /////////////////////////////////
  run(img) {
    img.loadPixels();
    this.findActiveNotes(img);
    this.drawActiveNotes(img);
  }
  /////////////////////////////////
  drawActiveNotes(img) {
    // draw active notes
    fill(255);
    noStroke();
    for (var i = 0; i < this.notes.length; i++) {
      for (var j = 0; j < this.notes[i].length; j++) {
        var alpha = this.notes[i][j].state * 200;
        var c1 = color(255, 0, 0, alpha);
        var c2 = color(0, 255, 0, alpha);
        var mix = lerpColor(c1, c2, map(i, 0, this.notes.length, 0, 1));
        this.notes[i][j].draw(mix);
      }
    }
  }
  /////////////////////////////////
  findActiveNotes(img) {
    for (var x = 0; x < img.width; x += 1) {
      for (var y = 0; y < img.height; y += 1) {
        var index = (x + (y * img.width)) * 4;
        var state = img.pixels[index + 0];
        if (state == 0) { // if pixel is black (ie there is movement)
          // find which note to activate
          var screenX = map(x, 0, img.width, 0, this.gridWidth);
          var screenY = map(y, 0, img.height, 0, this.gridHeight);
          var i = int(screenX / this.noteSize);
          var j = int(screenY / this.noteSize);
          this.notes[i][j].setActive();
        }
      }
    }
  }
}

// A custom Note class to manage displaying and playing each note on the grid
class Note {
  constructor(x, y, size, sound) {
    /**
     * x (float) - the x position of on the grid
     * y (float) - the y position of on the grid
     * size (float) - the size of the note
     * sound (p5.SoundFile) - the sound file to be played
    **/
    this.pos = createVector(x + size / 2, y + size / 2);
    this.size = size;
    this.state = 0;
    this.sound = sound;
  }

  draw(color) {
    if (this.state > 0) {
      var x = this.pos.x;
      var y = this.pos.y;
      fill(color);
      var s = this.state;
      ellipse(x, y, this.size * s, this.size * s);
    }
    this.state -= 0.05;
    this.state = constrain(this.state, 0, 1);
  }

  setActive() {
    this.state = 1;
    // Avoid playing the note again
    // before it finished playing the first time
    if (!this.sound.isPlaying()) {
      this.sound.play();
    }
  }

}