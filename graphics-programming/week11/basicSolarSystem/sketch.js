function setup() {
    createCanvas(800, 800, WEBGL);
}

function draw() {
    background(125);
    camera(0, -500, height, 0, 0, 0, 0, 1, 0);
    // sphere: no materials, no lights
    // sphere(100, 10, 10);

    // sphere: normal material, high res
    // normalMaterial();
    // sphere(100,100,100);

    // sphere: ambient material, light pointing towards the right
    // directionalLight(255,255,0, 1,0,0);
    // ambientMaterial(255);
    // sphere(100,100,100);

    // torus: ambient material, point light in the center and a bit towards you
    
    // Camera

    // Sun
    pointLight(255, 255, 255, 0, 0, 100);
    pointLight(255, 255, 255, 0, 0, 100);
    sphere(60);

    // Earth
    noStroke();
    ambientMaterial(255);
    rotateY(radians(frameCount));
    translate(300,0,0);
    sphere(40);


}
