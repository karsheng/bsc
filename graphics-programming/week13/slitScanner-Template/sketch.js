var video;

function setup() {
    createCanvas(640 * 2, 480);
    pixelDensity(1);
    video = createCapture(VIDEO);
    video.hide();
    // noLoop();
}

function draw() {
    image(video, 0, 0);

    // STEP 1 - write your cocde below
    const c = get();

    push();
    stroke(255, 0, 0);
    line(video.width / 2, 0, video.width / 2, video.height);
    pop();

    // STEP 2 - write your code below
    push();
    translate(300, 0);
    image(c, 0, 0);
    pop();

}
