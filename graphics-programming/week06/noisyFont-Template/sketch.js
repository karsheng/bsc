var points;
var font;

var amt = 80;

function preload() {
  font = loadFont("assets/Calistoga-Regular.ttf");
}

//////////////////////////////////////////////////////////////////////
function setup() {
  createCanvas(900, 400);
  background(0);

  points = font.textToPoints("c o d e", 50, 300, 300, {
    sampleFactor: 0.3,
    simplifyThreshold: 0,
  });
}

//////////////////////////////////////////////////////////////////////
function draw() {
  fill(0, 5);
  rect(0, 0, width, height);

  // **** Your code here ****
  noStroke();
  points.forEach((point) => {
    var nX = noise(frameCount + point.x + point.y);
    var amtX = map(mouseX, 0, width, 0, 80);
    var incX = map(nX, 0, 1, -amtX, amtX);
    var nY = noise(frameCount + point.x + point.y + 100);
    var amtY = map(mouseY, 0, height, 0, 80);
    var incY = map(nY, 0, 1, -amtY, amtY);
    var nS = map(nX, 0, 1, 5, 15);

    var nR = map(noise(100), 0, 1, 0, 255);
    var nG = map(noise(200), 0, 1, 0, 255);
    var nB = map(noise(300), 0, 1, 0, 255);

    fill(nR, nG, nB, point.alpha);
    ellipse(point.x + incX, point.y + incY, nS, nS);
  });
}
