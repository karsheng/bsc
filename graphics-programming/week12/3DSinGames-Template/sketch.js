const confLocs = [];
const confTheta = [];
var speedSlider;
var heightSlider;

function setup() {
  createCanvas(900, 800, WEBGL);
  for (var i = 0; i < 200; i++) {
    const x = random(-500, 500);
    const y = random(-800, 0);
    const z = random(-500, 500);
    confLocs.push(createVector(x, y, z));
    confTheta.push(random(0, 360));
  }
  speedSlider = createSlider(1, 30, 5);
  speedSlider.position(10, height + 20);
  let label1 = createElement('p', 'Speed Slider');
  label1.position(15, height + 30);

  heightSlider = createSlider(1, 300, 30);
  heightSlider.position(10, height + 90);
  let label2 = createElement('p', 'Height Slider');
  label2.position(15, height + 100);
}

function draw() {
  background(125);
  angleMode(DEGREES);
  fill(255);
  camera(
    1000 * cos((0.3 * frameCount) % 360),
    -600,
    1000 * sin((0.3 * frameCount) % 360),
    0,
    0,
    0,
    0,
    1,
    0
  );

  confetti();

  for (var i = 0; i < 16; i++) {
    push();
    const x = -400 + 50 * i;
    translate(x, 0, 0);
    for (var j = 0; j < 16; j++) {
      push();
      const z = -400 + 50 * j;
      var n = noise(x / 200, 0, z / 200);
      const distance = dist(x, z, 0, 0);
      const length = ((sin(distance + n * frameCount * speedSlider.value()) + 1) / 2) * 200  + heightSlider.value();
      translate(0, 0, z);
      normalMaterial();
      stroke(0);
      strokeWeight(2);
      box(50, length, 50);
      pop();
    }
    pop();
  }
}

function confetti() {
  confLocs.forEach((confLoc, i) => {
    const { x, y, z } = confLoc;
    push();
    normalMaterial();
    noStroke();
    translate(x, y, z);
    rotateX(confTheta[i]);
    // rotateY(confTheta[i]);
    plane(15, 15);
    confLocs[i].y += 1;
    confTheta[i] += 10;
    if (confLocs[i].y > 0) {
      confLocs[i].y = -800;
    }
    pop();
  })
}
