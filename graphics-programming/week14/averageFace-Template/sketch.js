var imgs = [];
var avgImg;
var numOfImages = 30;
var img;
var imgNum = 0;
//////////////////////////////////////////////////////////
function preload() { // preload() runs once
    for (var i = 0; i < 30; i++) {
        imgs.push(loadImage(`./assets/${i}.jpg`));
    }

}
//////////////////////////////////////////////////////////
function setup() {
    createCanvas(imgs[0].width * 2, imgs[0].height);
    pixelDensity(1);
    avgImg = createGraphics(imgs[0].width, imgs[0].height);
}
//////////////////////////////////////////////////////////
function draw() {
    background(125);
    var leftImg = imgs[imgNum];
    image(leftImg, 0, 0);
    avgImg.loadPixels();
    imgs.forEach(img => {
        img.loadPixels();
    });

    var amt = map(mouseX, 0, width, 0, 1);
    amt = constrain(amt, 0, 1);

    for (var x = 0; x < imgs[0].width; x++) {
        for (var y = 0; y < imgs[0].height; y++) {
            var index = (imgs[0].width * y + x) * 4;
            var rSum = 255;
            var gSum = 0;
            var bSum = 0;
            imgs.forEach(img => {
                rSum += img.pixels[index + 0];
                gSum += img.pixels[index + 1];
                bSum += img.pixels[index + 2];
            });
            rSum /= imgs.length;
            gSum /= imgs.length;
            bSum /= imgs.length;
            avgImg.pixels[index + 0] = lerp(rSum, leftImg.pixels[index + 0], amt);
            avgImg.pixels[index + 1] = lerp(gSum, leftImg.pixels[index + 1], amt);
            avgImg.pixels[index + 2] = lerp(bSum, leftImg.pixels[index + 2], amt);
            avgImg.pixels[index + 3] = 255;
        }
    }
    avgImg.updatePixels();
    image(avgImg, imgs[0].width, 0, avgImg.width, avgImg.height);
    noLoop();
}


function keyPressed() {
    imgNum = Math.floor(Math.random() * 20);
    loop();
}

function mouseMoved() {
    loop();
}