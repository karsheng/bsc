var font;
function preload() {
  font = loadFont("assets/Calistoga-Regular.ttf");
}

var points;

function setup() {
  createCanvas(900, 400);
  fill(255, 104, 204, 150);
  noStroke();

  points = font.textToPoints("c o d e", 50, 300, 300, {
    sampleFactor: 0.3,
    simplifyThreshold: 0,
  });

  console.log(points);
}

function draw() {
  background(0);

  // *** your code here ****
  points.forEach((point) => {
    var maxRange = map(mouseX, 0, width, 0, 100);
    ellipse(
      point.x + random(0, maxRange),
      point.y + random(0, maxRange),
      10,
      10
    );
  });

  noLoop();
}

function mouseMoved() {
  loop();
}
