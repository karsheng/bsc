def hash_with_extend(a, factor, extend):
    hash_table = [-1 for i in range(factor)]

    i = 0
    collision = True
    while collision:
        collision = False
        while i < len(a):
            v = a[i]
            hash = v%(int(factor))
            if hash_table[hash] == -1:
                hash_table[hash] = v
            else:
                collision = True
                load_factor = sum([1 if b != -1 else 0 for b in hash_table]) / len(hash_table)
                if load_factor > 0.5:
                    factor *= extend
                    hash_table = [-1 for i in range(int(factor))]
                    i = 0
                    break
                else:
                    a.pop(i)
                    i -= 1
            i += 1
    return hash_table

factor = 6

# a = [0,6,2,3,4,12]
# extend = 2
a = [10,26,12,43,54,17]
extend = 1.5

# print(hash_with_extend(a, factor, extend))

def hash_with_linear_probing(a, factor, hash_func = None):
    hash_table = [-1 for i in range(factor)]
    i = 0
    while i < len(a):
        v = a[i]
        if hash_func is None:
            hash = v%(int(factor))
        else:
            hash = hash_func(k=v)
        if hash_table[hash] == -1:
            hash_table[hash] = v
        else:
            j = 1
            while hash_table[(hash + j)%len(hash_table)] != -1:
                j += 1
            hash_table[(hash + j)%len(hash_table)] = v
            print(hash, (hash + j)%len(hash_table))
        i += 1

    return hash_table

# a = [10,26,12,43,54,17]
# a = [0,6,12,2,3,4]
# print(hash_with_linear_probing(a, 6))


def hash_with_separate_chaining(a, factor):
    hash_table = [[] for i in range(factor)]
    i = 0
    while i < len(a):
        v = a[i]
        hash = v%(int(factor))
        hash_table[hash].append(v)
        i += 1

    return hash_table
# a = [0,6,12,2,3,4]
a = [10,26,12,43,54,17]
# print(hash_with_separate_chaining(a, 6))


# print(hash_with_linear_probing([207, 126, 185, 31, 197, 79, 7], 8))
# print(hash_with_separate_chaining( [149, 176, 244, 66, 250], 8))

# print(hash_with_linear_probing([8,165,232,213,197],10))

# print(hash_with_separate_chaining([219,129,244,112,236,233,125],11))


def hash_func(k,a=31,c=37,m=50):
    return (a*k+c) % m


a = [2*i*i+5*i-5 for i in range(1,21)]


def load_factor(hash_table):
    return sum([1 if v != -1 else 0 for v in hash_table]) / len(hash_table) 

# hash_table = hash_with_linear_probing(a, 50, hash_func)
# print('ht', hash_table)
# lf = load_factor(hash_table)

# print('lf', lf)
# print(load_factor(hash_table))

# print(hash_with_linear_probing(a, 30, hash_func))

hash_with_linear_probing([238,40,128,59,212,185,215], 10, None)

def TraverseArray(A,N,i):
    for j in range(N):
        print(A[(i + j)%N])

A = [34, 4, 21, 7, 12]
# TraverseArray(A,N=5,i=1)


def func(H,N,k):
    i = (2*k) % N
    for j in range(N):
        if (H[(i + j)%N] == -1):
            H[(i + j)%N] = k
            break

H = [-1 for i in range(5)]
func(H,5,4)
func(H,5,9)
func(H,5,14)
# print('H', H)

# print(hash_with_separate_chaining([76,253,5,142], 9))