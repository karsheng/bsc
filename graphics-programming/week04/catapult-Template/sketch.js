// Example is based on examples from:
// http://brm.io/matter-js/
// https://github.com/shiffman/p5-matter
// https://github.com/b-g/p5-matter-examples

// module aliases
var Engine = Matter.Engine;
var Render = Matter.Render;
var World = Matter.World;
var Bodies = Matter.Bodies;
var Constraint = Matter.Constraint;

var engine;
var ground;

var ball1;
var ball2;

var catapult;
var catapultSpacer;
var constraint;

function setup() {
  createCanvas(800, 600);
  engine = Engine.create(); // create an engine
  setupCatapult();
  setupBalls();
  setupCatapultSpacer();
  setupGround();
}
/////////////////////////////////////////////////////////////
function draw() {
  background(0);
  Engine.update(engine);
  drawBalls();
  drawCatapult();
  drawCatapultSpacer();
  drawGround();
}
/////////////////////////////////////////////////////////////
function setupCatapult() {
  // your code here
  catapult = Bodies.rectangle(width / 2, height - 60, width / 1.5, 25, {
    isStatic: false,
  });
  var constraint1 = Constraint.create({
    pointA: { x: catapult.position.x, y: catapult.position.y },
    bodyB: catapult,
    stiffness: 1.0,
    length: 0,
  });

  World.add(engine.world, [catapult, constraint1]);
}
/////////////////////////////////////////////////////////////
function drawCatapult() {
  // your code here
  fill(255);
  drawVertices(catapult.vertices);
}

function setupCatapultSpacer() {
  catapultSpacer = Bodies.rectangle(
    catapult.position.x - width / 1.5 / 2 + 15,
    height - 35,
    30,
    25,
    {
      isStatic: true,
    }
  );
  World.add(engine.world, [catapultSpacer]);
}

function drawCatapultSpacer() {
  fill(255, 0, 0);
  drawVertices(catapultSpacer.vertices);
}

/////////////////////////////////////////////////////////////
function setupBalls() {
  // your code here
  ball1 = Bodies.circle(150, height - 60 - 20, 20, { density: 0.001 });
  ball2 = Bodies.circle(600, 0, 30, { density: 1 });
  World.add(engine.world, [ball1, ball2]);
}
/////////////////////////////////////////////////////////////
function drawBalls() {
  // your code here
  fill(0, 0, 255);
  drawVertices(ball1.vertices);
  drawVertices(ball2.vertices);
}
/////////////////////////////////////////////////////////////
function setupGround() {
  ground = Bodies.rectangle(400, height - 10, 810, 25, { isStatic: true });
  World.add(engine.world, [ground]);
}
/////////////////////////////////////////////////////////////
function drawGround() {
  noStroke();
  fill(128);
  drawVertices(ground.vertices);
}
////////////////////////////////////////////////////////////////
// ******* HELPER FUNCTIONS *********
// DO NOT WRITE BELOW HERE
/////////////////////////////////////////////////////////////
function drawVertices(vertices) {
  beginShape();
  for (var i = 0; i < vertices.length; i++) {
    vertex(vertices[i].x, vertices[i].y);
  }
  endShape(CLOSE);
}
