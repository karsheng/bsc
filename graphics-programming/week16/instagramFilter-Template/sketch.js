// Image of Husky Creative commons from Wikipedia:
// https://en.wikipedia.org/wiki/Dog#/media/File:Siberian_Husky_pho.jpg
var imgIn;

// This selects the filter in the filters array and changes with
// key pressed.
var filterSelected = 0;

// The different filter functions are stored in an array
var filters = [
  earlyBirdFilter, 
  greyscaleFilter, 
  invertFilter, 
  edgeDetectionFilter,
  sharpenFilter
];

/////////////////////////////////////////////////////////////////
function preload() {
  imgIn = loadImage("assets/husky.jpg");
}
/////////////////////////////////////////////////////////////////
function setup() {
  createCanvas((imgIn.width * 2), imgIn.height + 150);
  pixelDensity(1);
}
/////////////////////////////////////////////////////////////////
function draw() {
  background(255);
  image(imgIn, 0, 0);
  image(filters[filterSelected](imgIn), imgIn.width, 0);
  var margin = 20;
  text("Early Bird Filter = Press 1", 20, imgIn.height + margin + 10);
  text("Gray Scale Filter = Press 2", 20, imgIn.height + margin + 30);
  text("Invert Filter = Press 3", 20, imgIn.height + margin + 50);
  text("Edge Detection Filter = Press 4", 20, imgIn.height + margin + 70);
  text("Shapen Filter = Press 5", 20, imgIn.height + margin + 90);
  noLoop();
}
/////////////////////////////////////////////////////////////////
function mousePressed() {
  loop();
}

function keyPressed() {
  if (keyCode === 49 || keyCode === 97) { // key 1
    filterSelected = 0;
  } else if (keyCode === 50 || keyCode === 98) { // key 2
    filterSelected = 1;
  } else if (keyCode === 51 || keyCode === 99) { // key 3
    filterSelected = 2;
  } else if (keyCode === 52 || keyCode === 100) {// key 4
    filterSelected = 3;
  } else if (keyCode === 53 || keyCode === 101) {// key 4
    filterSelected = 4;
  }
  loop();
}

/*
The convolution function
*/
function convolution(x, y, matrix, matrixSize, img) {
  var totalRed = 0.0;
  var totalGreen = 0.0;
  var totalBlue = 0.0;
  var offset = floor(matrixSize / 2);

  // convolution matrix loop
  for (var i = 0; i < matrixSize; i++) {
      for (var j = 0; j < matrixSize; j++) {
          // Get pixel loc within convolution matrix
          var xloc = x + i - offset;
          var yloc = y + j - offset;
          var index = (xloc + img.width * yloc) * 4;
          // ensure we don't address a pixel that doesn't exist
          index = constrain(index, 0, img.pixels.length - 1);

          // multiply all values with the mask and sum up
          totalRed += img.pixels[index + 0] * matrix[i][j];
          totalGreen += img.pixels[index + 1] * matrix[i][j];
          totalBlue += img.pixels[index + 2] * matrix[i][j];
      }
  }
  // return the new color
  return [totalRed, totalGreen, totalBlue];
}

/*
All the filters 
*/
function earlyBirdFilter(img) {
  var resultImg = createImage(img.width, img.height);
  resultImg = sepiaFilter(img);
  resultImg = darkCorners(resultImg);
  resultImg = radialBlurFilter(resultImg);
  resultImg = borderFilter(resultImg)
  return resultImg;
}

function sepiaFilter(img) {
  imgOut = createImage(img.width, img.height);

  imgOut.loadPixels();
  img.loadPixels();

  for (var x = 0; x < imgOut.width; x++) {
    for (var y = 0; y < imgOut.height; y++) {

      var index = (x + y * imgOut.width) * 4;

      var oldRed = img.pixels[index + 0];
      var oldGreen = img.pixels[index + 1];
      var oldBlue = img.pixels[index + 2];
      var newRed = (oldRed * .393) + (oldGreen * .769) + (oldBlue * .189)
      var newGreen = (oldRed * .349) + (oldGreen * .686) + (oldBlue * .168)
      var newBlue = (oldRed * .272) + (oldGreen * .534) + (oldBlue * .131)

      imgOut.pixels[index + 0] = newRed;
      imgOut.pixels[index + 1] = newGreen;
      imgOut.pixels[index + 2] = newBlue;
      imgOut.pixels[index + 3] = 255;
    }
  }
  imgOut.updatePixels();
  return imgOut;
}


function darkCorners(img) {
  imgOut = createImage(img.width, img.height);

  imgOut.loadPixels();
  img.loadPixels();

  var centerX = img.width / 2;
  var centerY = img.height / 2;
  
  for (var x = 0; x < imgOut.width; x++) {
    for (var y = 0; y < imgOut.height; y++) {
      var index = (x + y * imgOut.width) * 4;
      var dynLum;
      var pixelDist = dist(x, y, centerX, centerY);
      if (pixelDist < 300) {
        dynLum = 1;
      } else if (pixelDist >= 300 && pixelDist < 450) {
        dynLum = map(pixelDist, 300, 450, 1, 0.4);
      } else {
        dynLum = map(pixelDist, 450, (centerX**2 + centerY**2)**0.5, 0.4, 0);
      }
      var oldRed = img.pixels[index + 0];
      var oldGreen = img.pixels[index + 1];
      var oldBlue = img.pixels[index + 2];
      var newRed = constrain(oldRed * dynLum, 0, 255);
      var newGreen = constrain(oldGreen * dynLum, 0, 255);
      var newBlue = constrain(oldBlue * dynLum, 0, 255);

      imgOut.pixels[index + 0] = newRed;
      imgOut.pixels[index + 1] = newGreen;
      imgOut.pixels[index + 2] = newBlue;
      imgOut.pixels[index + 3] = 255;
    }
  }
  imgOut.updatePixels();
  return imgOut;
}

function radialBlurFilter(img){
  var imgOut = createImage(img.width, img.height);
  var matrix = [
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64],
    [1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64, 1 / 64]
  ];
  var matrixSize = matrix.length;

  imgOut.loadPixels();
  img.loadPixels();

  // read every pixel
  for (var x = 0; x < imgOut.width; x++) {
      for (var y = 0; y < imgOut.height; y++) {

          var index = (x + y * imgOut.width) * 4;
          var c = convolution(x, y, matrix, matrixSize, img);
          var r = img.pixels[index + 0];
          var g = img.pixels[index + 1];
          var b = img.pixels[index + 2];

          var dynBlur = map(dist(mouseX, mouseY, img.width/2, img.height/2), 100, 300, 0, 1);
          dynBlur = constrain(dynBlur, 0, 1);
          
          imgOut.pixels[index + 0] = c[0]*dynBlur + r*(1-dynBlur);
          imgOut.pixels[index + 1] = c[1]*dynBlur + g*(1-dynBlur);
          imgOut.pixels[index + 2] = c[2]*dynBlur + b*(1-dynBlur);
          imgOut.pixels[index + 3] = 255;
      }
  }
  imgOut.updatePixels();
  return imgOut;
}

function borderFilter(img) {
  var buffer = createGraphics(img.width, img.height);
  buffer.loadPixels();

  for (var x = 0; x < buffer.width; x++) {
    for (var y = 0; y < buffer.height; y++) {
      var index = (buffer.width * y + x) * 4;
      buffer.pixels[index + 0] = img.pixels[index + 0];
      buffer.pixels[index + 1] = img.pixels[index + 1];
      buffer.pixels[index + 2] = img.pixels[index + 2];
      buffer.pixels[index + 3] = img.pixels[index + 3];
    }
  }
  buffer.updatePixels();
  buffer.stroke(255);
  buffer.strokeWeight(25);
  buffer.noFill();
  buffer.rectMode(CENTER);
  buffer.rect(buffer.width/2, buffer.height/2, buffer.width-25, buffer.height-25, 25);
  buffer.rect(buffer.width/2, buffer.height/2, buffer.width-25, buffer.height-25);
  return buffer;
}

function greyscaleFilter(img){
  var imgOut = createImage(img.width, img.height);
  imgOut.loadPixels();
  img.loadPixels();

  for (x = 0; x < imgOut.width; x++) {
      for (y = 0; y < imgOut.height; y++) {

          var index = (x + y * imgOut.width) * 4;

          var r = img.pixels[index + 0];
          var g = img.pixels[index + 1];
          var b = img.pixels[index + 2];

          var gray = r * 0.299 + g * 0.587 + b * 0.0114; // LUMA ratios

          imgOut.pixels[index+0]= imgOut.pixels[index+1] = imgOut.pixels[index+2] = gray;
          imgOut.pixels[index+3]= 255;
      }
  }
  imgOut.updatePixels();
  return imgOut;
}

function invertFilter(img){
  imgOut = createImage(img.width, img.height);

  imgOut.loadPixels();
  img.loadPixels();

  for (var x = 0; x < imgOut.width; x++) {
      for (var y = 0; y < imgOut.height; y++) {

          var index = (x + y * imgOut.width) * 4;

          var r = 255 - img.pixels[index + 0];
          var g = 255 - img.pixels[index + 1];
          var b = 255 - img.pixels[index + 2];

          imgOut.pixels[index + 0] = r;
          imgOut.pixels[index + 1] = g;
          imgOut.pixels[index + 2] = b;
          imgOut.pixels[index + 3] = 255;
      }
  }
  imgOut.updatePixels();
  return imgOut;
}

function edgeDetectionFilter(img){
  var imgOut = createImage(img.width, img.height);
  //horizontal edge detection / vertical lines
  var matrixX = [    // in javascript format
    [-1, -2, -1],
    [0, 0, 0],
    [1, 2, 1]
  ];
  //vertical edge detection / horizontal lines
  var matrixY = [
    [-1, 0, 1],
    [-2, 0, 2],
    [-1, 0, 1]
  ];
  var matrixSize = matrixX.length;

  imgOut.loadPixels();
  img.loadPixels();

  // read every pixel
  for (var x = 0; x < imgOut.width; x++) {
      for (var y = 0; y < imgOut.height; y++) {

          var index = (x + y * imgOut.width) * 4;
          var cX = convolution(x, y, matrixX, matrixSize, img);
          var cY = convolution(x, y, matrixY, matrixSize, img);

          cX = map(abs(cX[0]), 0, 1020, 0, 255);
          cY = map(abs(cY[0]), 0, 1020, 0, 255);
          var combo = cX + cY;

          imgOut.pixels[index + 0] = combo;
          imgOut.pixels[index + 1] = combo;
          imgOut.pixels[index + 2] = combo;
          imgOut.pixels[index + 3] = 255;
      }
  }
  imgOut.updatePixels();
  return imgOut;
}

function sharpenFilter(img){
  var imgOut = createImage(img.width, img.height);
  var matrix = [
    [-1, -1, -1],
    [-1, 9, -1],
    [-1, -1, -1]
  ];
  var matrixSize = matrix.length;

  imgOut.loadPixels();
  img.loadPixels();

  // read every pixel
  for (var x = 0; x < imgOut.width; x++) {
      for (var y = 0; y < imgOut.height; y++) {

          var index = (x + y * imgOut.width) * 4;
          var c = convolution(x, y, matrix, matrixSize, img);

          imgOut.pixels[index + 0] = c[0];
          imgOut.pixels[index + 1] = c[1];
          imgOut.pixels[index + 2] = c[2];
          imgOut.pixels[index + 3] = 255;
      }
  }
  imgOut.updatePixels();
  return imgOut;
}